//
//  WebSocketService.swift
//  WebSockets
//
//  Created by Jansen Ducusin on 6/1/21.
//

import Starscream

protocol WebSocketServiceDelegate:AnyObject {
    func newMessages(message: String)
}

class WebSocketService{
    static let shared = WebSocketService()
    
    var socket: WebSocket!
    
    private var isConnected = false
    
    weak var delegate: WebSocketServiceDelegate?
    
    init(){
        var request = URLRequest(url: URL(string: "https://jarvis-nodejs.herokuapp.com")!)
        request.timeoutInterval = 5
        
        socket = WebSocket(request: request)
        socket.connect()
        listenToSocket()
    }
    
    func sendMessageToServer(_ message: String){
        socket.write(string: message) {
            print("DEBUG: message sent!")
        }
    }
    
    private func listenToSocket(){
        socket.onEvent = { [weak self] event in
            switch event {

            case .connected(let headers):
                self?.isConnected = true
                print("websocket is connected: \(headers)")
            case .disconnected(let reason, let code):
                self?.isConnected = false
                print("websocket is disconnected: \(reason) with code: \(code)")
            case .text(let string):
                print("Received text: \(string)")
                self?.delegate?.newMessages(message: string)
            case .binary(let data):
                print("Received data: \(data.count)")
            case .ping(_):
                break
            case .pong(_):
                break
            case .viabilityChanged(_):
                break
            case .reconnectSuggested(_):
                break
            case .cancelled:
                self?.isConnected = false
            case .error(let error):
                if let error = error{
                    print(error.localizedDescription)
                    return
                }
                self?.isConnected = false

            }
        }
    }
}
