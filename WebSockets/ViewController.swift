//
//  ViewController.swift
//  WebSockets
//
//  Created by Jansen Ducusin on 6/1/21.
//

import UIKit


class ViewController: UIViewController {
    
    // MARK: - Properties
    private lazy var inputTextField: UITextField = {
        let textField = UITextField()
        textField.setHeight(height: 30)
        textField.placeholder = "Your message here..."
        return textField
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.tableFooterView = UIView()
        return tableView
    }()
    
    private lazy var divider: UIView = {
        let dividerView = UIView()
        dividerView.setHeight(height: 0.75)
        dividerView.backgroundColor = .opaqueSeparator
        return dividerView
    }()
    
    private lazy var sendButton: UIButton = {
        let button = UIButton()
        let image = UIImage(systemName: "arrowtriangle.right.fill")?.withRenderingMode(.alwaysTemplate)
        button.setImage(image, for: .normal)
        button.tintColor = .darkGray
        button.setWidth(width: 50)
        button.addTarget(self, action: #selector(sendMessage), for: .touchUpInside)
        return button
    }()
    
    private var logs = [String]()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
        WebSocketService.shared.delegate = self
    }
    
    // MARK: - Selectors
    @objc private func sendMessage(){
        guard let message = inputTextField.text,
              !message.isEmpty else {return}
        
        WebSocketService.shared.sendMessageToServer(message)
    }
    
    // MARK: - Helpers
    private func setupUI(){
        view.backgroundColor = .white
        
        let textFieldStack = UIStackView(arrangedSubviews: [inputTextField, sendButton])
        view.addSubview(textFieldStack)
        
        let containerStack = UIStackView(arrangedSubviews: [textFieldStack, divider, tableView])
        containerStack.axis = .vertical
        containerStack.spacing = 10
        view.addSubview(containerStack)
        containerStack.anchor(
            top: view.safeAreaLayoutGuide.topAnchor,
            left: view.safeAreaLayoutGuide.leftAnchor,
            bottom: view.safeAreaLayoutGuide.bottomAnchor,
            right: view.safeAreaLayoutGuide.rightAnchor,
            paddingTop: 10,
            paddingLeft: 10,
            paddingRight: 10
        )
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        logs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = logs[indexPath.row]
        return cell
    }
}

extension ViewController: WebSocketServiceDelegate {
    func newMessages(message: String) {
        logs.append(message)
        tableView.reloadData()
    }
    
}
